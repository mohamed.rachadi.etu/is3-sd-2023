/* rationnel. c */

#include <stdio.h>
#include "rationnel.h"


/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

/* Constructeur. Affecte p/q à R. On suppose q non nul */
void init_rationnel (struct rationnel* R, int p, int q)
{   int g;

    g = Euclide (p, q);
    if (q < 0)
    {   R->numer = - p / g;
        R->denom = - q / g;
    } else
    {   R->numer = p / g;
        R->denom = q / g;
    }
}

/* Destructeur */
void clear_rationnel (struct rationnel* R)
{
}

/* Affecte A + B à S */
void add_rationnel (struct rationnel* S, 
                           struct rationnel* A, struct rationnel* B)
{   int p, q;
    p = A->numer * B->denom + A->denom * B->numer;
    q = A->denom * B->denom;
    init_rationnel (S, p, q);   /* faute de goût qui va se traduire
                                    par un bug */
}

/* Affiche son paramètre */
void print_rationnel (struct rationnel* R)
{
    if (R->denom == 1)
        printf ("%d\n", R->numer);
    else
        printf ("%d/%d\n", R->numer, R->denom);
}


