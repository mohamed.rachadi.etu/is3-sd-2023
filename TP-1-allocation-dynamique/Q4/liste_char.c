#include <stdio.h>
#include <stdlib.h>
#include "liste_char.h"


void init_liste_char(struct liste_char * L){
     L->tete= NIL;
     L->nbelem=0;
}

void ajout_en_queue_liste(struct liste_char *L, char c){
     struct maillon_char *M,*N;
     N=( struct maillon_char *) malloc (sizeof(struct maillon_char));
     N->value=c;
     N->suivant=NIL;
     if (L->nbelem==0){
        L->tete=N;
     }
     else {
           M=L->tete;
           while (M->suivant!=NIL){
                  M=M->suivant;
           }
           M->suivant=N;
     
     
     }
     L->nbelem+=1;

}
void print_liste(struct liste_char *L){
     printf("[");
     struct maillon_char * M;
     M=L->tete;
     for (int i=0;i<L->nbelem;i++){
          printf("%c",M->value);
          M=M->suivant;
     }    
     printf("]\n");
}
void clear_liste(struct liste_char* L){
     struct maillon_char *M,*N;
     M=L->tete;
     for (int i=0;i<L->nbelem;i++){
         N=M->suivant;
         free(M);
         M=N;
     
     }

}


