#if ! defined (GRAHAM_H)
#define GRAHAM_H 1

#include <stdbool.h>

#include "liste_point.h"

extern bool trie_points (int, struct point *);

extern void Graham (int *, struct point *, int, struct point *);

#endif
