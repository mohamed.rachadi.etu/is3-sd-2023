/* point.h */


#include <stdbool.h>
struct point {
       double x; /* abscisse */
       double y; /* ordonnée */
       char ident; /* identificateur */
};
/*Specification */ 
/* la structure point est défini par deux doubles qui sont l'abscisse et l'ordonnée ainsi qu'un identificateur qui est un caractere */ 


extern void init_point (struct point*, double, double, char);
extern int compare_points (const void*, const void*);
extern bool tourne_a_gauche (struct point*, struct point*, struct point*);
