#include <stdlib.h>

/*
 * a0 = l'adresse d'un élt de T
 * b0 = l'adresse d'un élt de T
 *
 * On retourne -1 si a doit être avant b dans T
 *              1                après
 */

struct point
{   double x;
    double y;
    char ident;
};

int compare_point (const void* a0, const void* b0)
{   struct point a = *(struct point*)a0; 
                            // a = l'étoile de a0, vu comme un struct point *
    struct point b = *(struct point*)b0; // pareil

    double det = a.x * b.y - b.x * a.y;

    if (det > 0)
        return -1;
    else if (det == 0)
        return 0;
    else
        return 1;
}

int main ()
{   struct point T [] = { { 6, 6, 'D' }, { 4, 3, 'A' }, 
                          { 8, 1, 'B' }, { 1, 5, 'C' } };
    int n = sizeof (T) / sizeof (struct point);

    qsort (T, n, sizeof (struct point), &compare_point);

    return 0;
}

