#include <stdlib.h>

/*
 * a0 = l'adresse d'un élt de T
 * b0 = l'adresse d'un élt de T
 *
 * On retourne -1 si a doit être avant b dans T
 *              1                après
 */

int compare_double (const void* a0, const void* b0)
{   double a = *(double*)a0; // a = l'étoile de a0, vu comme un double *
    double b = *(double*)b0; // pareil

    if (a < b)
        return -1;
    else if (a == b)
        return 0;
    else
        return 1;
}

int main ()
{   double T [] = { 37, 23, -5, 144, 8, -13 };
    int n = sizeof (T) / sizeof (double);

    qsort (T, n, sizeof (double), &compare_double);

    return 0;
}

