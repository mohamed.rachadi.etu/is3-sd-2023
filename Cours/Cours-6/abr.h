/* abr.h */

struct abr {
    struct abr* gauche; // Par ici il n'y a que des doubles < valeur
    double valeur;      // valeur est une clé
    struct abr* droite; // Par ici il n'y a que des doubles > valeur
};

#define NIL (struct abr*)0

// Un ABR = un pointeur de type (struct abr*)

// Pas de constructeur - on initialise le pointeur à NIL

// Le destructeur
extern void clear_abr (struct abr*);

// Ajout d'un double dans un ABR. 
// La fonction retourne l'ABR obtenu après l'ajout
extern    struct abr*      ajout_abr (struct abr*, double);

// Autre prototype possible
// extern void ajout_abr (struct abr**, double);

#include <stdbool.h>

// Retourne true si le double appartient à l'ABR, false sinon
extern bool recherche_abr (double, struct abr*);

