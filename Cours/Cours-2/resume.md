Résumé du cours 2
-----------------

Quand on affecte une valeur à une variable v, on distingue deux cas

1. La variable n'a pas été initialisée. Plus généralement, son contenu
   est "aléatoire" et est incohérent

2. La variable a déjà une valeur (elle est dans un état cohérent) et on
   veut changer cette valeur

Def. Un constructeur est une action/fonction pour le cas 1.

Rq. Un type peut avoir plusieurs constructeurs

Rq. Convention du cours : actions init_le_nom_du_type

Destructeur. Un par type. Une variable est sur le point de disparaître
    (= de devenir inaccessible). Par exemple : variable locale à une
    fonction et on est sur le point de sortir de la fonction.

    Le destructeur = une action qui joue le rôle d'une équipe de 
    nettoyage et qui recycle tout ce qui doit l'être avant que la
    variable disparaisse.

Rq. Convention du cours : actions clear_le_nom_du_type

Allocation dynamique

Problème à résoudre : dans beaucoup d'applications on a besoin de
    manipuler des variables qui consomment de la mémoire mais
    le volume mémoire nécessaire n'est connu qu'à l'exécution

    Et on ne veut/peut pas surdimensionner la variable.

    Allocation de mémoire. 
    Dynamique (= à l'exécution) par opposition à connu à la compilation





























