#include <stdlib.h>
#include <stdio.h>
#include "chaine.h"
#include <ctype.h>

int main(){
    struct chaine S;
    int c;
    init_chaine(&S);
    c=getchar();
    while (!isspace(c)){
        ajout_en_queue_chaine(&S, (char) c);
        c=getchar();
    }
    print_chaine(&S);
    clear(&S);
    return 0;
}

