/* rationnel. c */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "chaine.h"


/* constructeur : initialise la chaine a une chaine vide */ 

void init_chaine(struct chaine* S){
     S->tab=(char*)malloc(64*sizeof(char));
     (S->tab)[0]='\0';
     S->l=0;
}
/* Ajoute le caractere c en queue de notre chaine */
void ajout_en_queue_chaine(struct chaine* S, char c){
     int i=S->l;
     (S->tab)[i]=c;
     (S->l)++;

}

/* Affiche la chaine  */

void print_chaine(struct chaine* S){
      printf("%s\n",S->tab);     
}


void clear(struct chaine* S){
     free(S->tab);
     S->l=0;

}
