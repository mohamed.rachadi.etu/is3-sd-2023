/* rationnel. c */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "chaine.h"



/* constructeur : initialise la chaine a une chaine vide */ 

void init_chaine(struct chaine* S){
     init_liste_char(& (S->L) );
}
/* Ajoute le caractere c en queue de notre chaine */
void ajout_en_queue_chaine(struct chaine* S, char c){
     ajout_en_queue_liste(&S->L,c);

}

/* Affiche la chaine  */

void print_chaine(struct chaine* S){
      print_liste(&S->L);     
}


void clear(struct chaine* S){
     clear_liste(&S->L);

}



