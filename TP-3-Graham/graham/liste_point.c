#include <stdio.h>
#include <stdbool.h> 
#include "liste_point.h"
#include <assert.h>

void init_liste_point (struct liste_point *L){
     L->tete=NIL;
     L->nbelem=0;     

}
void clear_liste(struct liste_point* L){
     struct maillon_point *M,*N;
     M=L->tete;
     for (int i=0;i<L->nbelem;i++){
         N=M->next;
         free(M);
         M=N;
     
     }

}
void ajouter_en_tete_liste_point (struct liste_point *L, struct point P)
{
    struct maillon_point *nouveau;

    nouveau =(struct maillon_point *) malloc (sizeof (struct maillon_point));
    assert (nouveau != (struct maillon_point *) 0);
/* appeler ici un éventuel constructeur pour nouveau->value */
    nouveau->value = P;         /* affectation de la valeur */
    nouveau->next = L->tete;
    L->tete = nouveau;
    L->nbelem += 1;
}


int est_vide_liste(struct liste_point *L){
    return (L->nbelem==0);
}

void extraire_tete_liste_point (struct point  *P, struct liste_point * L){
     if (est_vide_liste(L)){
        printf("la liste est vide y'a rien a depiler");
     
     }
     else {
           struct maillon_point *tete;

           
           tete = L->tete;
           *P = tete->value;           /* affectation */
           L->tete = tete->next;
           L->nbelem -= 1;
           free(tete);

     
     }


}


