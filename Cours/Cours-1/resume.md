Résumé du cours
---------------

Pour implanter un type "nombre rationnel" on écrit deux fichiers :
- un fichier rationnel.h (header / en-tête)
- un fichier rationnel.c (source C)

Le fichier .h contient :
- déclaration du type (struct C)
- spécifications du type
- prototypes des primitives exportées de manipulation du type avec
    leur spécification

Un logiciel est composé de plusieurs fichiers .c et .h
- on n'inclut que les '.h'
- on ne compile que les '.c'

Compilation séparée :
- on peut compiler les '.c' séparément :
    gcc -c main.c
    gcc -c rationnel.c
  on obtient des fichiers objets (suffixe '.o')
  ce sont des fichiers en langage machine avec des "trous" pour les
    références vers les fonctions écrites dans les autres '.c'
- quand on a tous les '.o' du logiciel, on peut finir la compilation
  et obtenir un exécutable. C'est l'étape d'édition des liens (linker
  en Anglais)
    gcc rationnel.o main.o
  Par défaut, l'exécutable s'appelle 'a.out'



