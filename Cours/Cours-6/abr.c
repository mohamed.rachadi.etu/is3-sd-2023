/* abr.c */

#include <stdlib.h>
#include "abr.h"

bool recherche_abr (double d, struct abr* A0)
{
    struct abr* A = A0; 
    bool found = false;
// Pas de malloc ! A est un curseur qui se déplace dans l'ABR
    while (A != NIL && !found)
    {
        if (d < A->valeur)
            A = A->gauche;
        else if (d == A->valeur)
            found = true;
        else
            A = A->droite;
    }
    return found;
}

// Fonction auxiliaire qui construit une feuille
static struct abr* new_feuille (double d)
{   struct abr* F;
    F = (struct abr*)malloc (sizeof (struct abr));
    F->gauche = NIL;
    F->droite = NIL;
    F->valeur = d;
    return F;
}

struct abr* ajout_abr (struct abr* A0, double d)
{   struct abr* cour;
    struct abr* prec;
    if (A0 == NIL)
        return new_feuille (d);
// On est sûr que l'ABR n'est pas vide
    prec = NIL; 
    cour = A0;
    while (cour != NIL)
    {   prec = cour;
// Pas d'égalité parce que d est une clé
        if (d < cour->valeur)
            cour = cour->gauche;
        else
            cour = cour->droite;
    }
    if (d < prec->valeur)
        prec->gauche = new_feuille (d);
    else
        prec->droite = new_feuille (d);
    return A0;
}










