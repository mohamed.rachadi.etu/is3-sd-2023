/* rationnel.h */

/* 1. La déclaration */

struct rationnel {
    int numer;
    int denom;
};

/* 2. Spécifications du type 

   Le type struct rationnel permet de représenter des nombres rationnels
   Le champ numer contient le numérateur
   Le champ denom contient le dénominateur. Le champ denom est non nul.

   Les fractions sont réduites : le pgcd de numer et denom est égal à 1.
   Le champ denom est strictement positif.
 */

/* 3. Les prototypes des primitives de manipulation du type */

/* Constructeur. Affecte p/q à R */
extern void init_rationnel (struct rationnel* R, int p, int q);

/* Destructeur */
extern void clear_rationnel (struct rationnel*);

/* Affecte A + B à S */
extern void add_rationnel (struct rationnel* S, 
                           struct rationnel* A, struct rationnel* B);

/* Affiche son paramètre */
extern void print_rationnel (struct rationnel*);





