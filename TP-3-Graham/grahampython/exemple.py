#! /usr/bin/python3
import matplotlib.pyplot as plt
from geoalg import Graham

G = Graham ()
G.points_aleatoires (n = 20, seed = 73738)
G.points_aleatoires (n = 10, seed = 73738)
P = G.get_points ()
E = G.get_enveloppe ()

for p in P :
    plt.scatter (p[0], p[1])
    plt.annotate (p[2], (p[0]+5e-3, p[1]+5e-3))

plt.plot ([e[0] for e in E], [e[1] for e in E])
plt.savefig ("Graham.png")
plt.show ();

