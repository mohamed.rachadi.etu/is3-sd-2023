#include <stdio.h>
#include <stdbool.h> 
#include "point.h"

extern void init_point (struct point *P, double x1, double y1, char C){
            P->x=x1;
            P->y=y1;
            P->ident=C;
}

extern int compare_points (const void *P1, const void *P2){
           struct point p1=*(struct point *)P1;
           struct point p2=*(struct point *)P2;
           double det=(p1.x)*(p2.y)-p1.y*p2.x;
           if (det>0){
              return -1; 
           }
           else if (det==0){
                return 0;
           }
           else 
               return 1;
}

extern bool tourne_a_gauche (struct point*A, struct point*B, struct point*C){
            double xu=B->x-A->x;
            double yu=B->y-A->y;
            double xv=C->x-A->x;
            double yv=C->y-A->y;
            double det= xu*yv-xv*yu;
            if (det>=0){
               return true;
            }
            else 
                return false ;
}
