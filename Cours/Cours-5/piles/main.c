/* main.c */

#include <stdio.h>
#include "pile.h"

int main ()
{   double data [] = { 18, 421, -42, 34, -3 };
    int nb_data = sizeof (data) / sizeof (double);

    struct pile P;

    init_pile (&P);
    for (int i = 0; i < nb_data; i++)
    {   printf ("on empile %lf\n", data[i]);
        empiler (&P, data[i]);
    }
    while (! est_vide_pile (&P))
    {   double d;
        depiler (&d, &P);
        printf ("on depile %lf\n", d);
    }
    clear_pile (&P);
    return 0;
}

