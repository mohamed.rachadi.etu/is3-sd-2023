#include <stdlib.h>
#include <stdbool.h>

struct point
{   double x;
    double y;
    char ident;
};

/* 
 * Retourne true si, en allant de A vers C en passant par B, on
 * tourne à gauche en B
 */

bool tourne_a_gauche (struct point* A, struct point* B, struct point* C)
{   double xu = B->x - A->x,
           yu = B->y - A->y,
           xv = C->x - A->x,
           yv = C->y - A->y,
          det = xu * yv - yu * xv;
    if (det >= 0)
        return true;
    else
        return false;
}

int main ()
{   struct point C = { 10, 1, 'C' },
                 H = {  8, 3, 'H' },
                 I = { 10, 9, 'I' };
    bool b;

    b = tourne_a_gauche (&C, &H, &I);

    return 0;
}

