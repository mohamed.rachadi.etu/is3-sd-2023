
/**********************************************************************
 * IMPLANTATION
 * 
 * Spécification de l'implantation
 * 
 * Implantation des listes   simplement chaînées de caracteres
 * 
 * Les maillons sont alloués dynamiquement. 
 * Le champ suivant  du dernier maillon vaut (struct maillon_char*)''
 * 
 * Le champ tete d'une liste pointe vers le premier maillon
 * Le champ nbelem est égal au nombre de maillons de la liste
 * La liste vide est codée par (tete, nbelem) = ((struct maillon_char*)'', 0)
 *
 * Des listes distinctes ont des maillons distincts (pas de maillon partagé).
 **********************************************************************/
 
#define  	NIL (struct maillon_char *)0  

struct maillon_char{
       char value;
       struct maillon_char * suivant ;

};

struct liste_char{
       struct maillon_char* tete;
       int nbelem;

};

/* 3. Les prototypes des primitives de manipulation du type */

/* Constructeur. initialise la chaine a une chaine vide */
extern void init_liste_char(struct liste_char * );

/* Ajoute le caractere char en queue de notre chaine */
extern void ajout_en_queue_liste(struct liste_char*, char);

/* Affiche la chaine  */
extern void print_liste(struct liste_char *);

/* Destructeur */
extern void clear_liste(struct liste_char *);
