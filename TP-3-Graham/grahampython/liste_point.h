#include <stdio.h>
#include <stdlib.h>


#include "point.h"


#define NIL (struct maillon_point *) 0 



/**********************************************************************
 * IMPLANTATION
 * 
 * Spécification de l'implantation
 * 
 * Implantation des listes de point pour la pile 
 * 
 *  
 * Le champ L est notre tableau de taille N 
 * 
 * Le champ sp fait reference a l'indice de 1ere case vide dans la pile 
 * la pile vide est codé par sp=0;
 *
 * 
 **********************************************************************/


struct maillon_point
{
    struct point  value;
    struct maillon_point *next;
};

struct liste_point
{
    struct maillon_point *tete;
    int nbelem;
};


/**********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 **********************************************************************/

/* Constructeur. Initialise son paramètre à la pile vide  */ 
extern int  est_vide_liste(struct liste_point *);

extern void init_liste_point (struct liste_point *);

/*Destructeur*/ 

extern void clear_liste_point (struct liste_point *);

/*Ajout d'un double en tête de liste : empiler  */ 

extern void ajouter_en_tete_liste_point (struct liste_point *, struct point);

/* depiler */ 

extern void extraire_tete_liste_point (struct point *, struct liste_point *);

