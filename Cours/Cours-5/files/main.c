/* main.c */

#include <stdio.h>
#include "file.h"

int main ()
{   double data [] = { 18, 421, -42, 34, -3 };
    int nb_data = sizeof (data) / sizeof (double);

    struct file F;

    init_file (&F);
    for (int i = 0; i < nb_data; i++)
    {   printf ("on enfile %lf\n", data[i]);
        enfiler (&F, data[i]);
    }
    while (! est_vide_file (&F))
    {   double d;
        defiler (&d, &F);
        printf ("on defile %lf\n", d);
    }
    clear_file (&F);
    return 0;
}

