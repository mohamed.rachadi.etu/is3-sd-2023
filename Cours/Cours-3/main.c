/* main.c */

#include "liste.h"

int main ()
{   struct liste L0;

    init_liste (&L0);   // initialise à la liste vide
    ajout_en_tete_liste (&L0, 421); // ajoute 421 en tête de L0
    ajout_en_tete_liste (&L0, -22); // ajoute -22 en tête de L0
    ajout_en_tete_liste (&L0, 8); // ajoute 8 en tête de L0
    print_liste (&L0); // affiche [8, -22, 421]
    clear_liste (&L0); // Le destructeur libère les maillons (3 free)
    return 0;
}

