/* pile.c */

#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
    P->sp = 0;
}

void clear_pile (struct pile* P)
{
}

bool est_vide_pile (struct pile* P)
{
    return P->sp == 0;
}

void empiler (struct pile* P, double d)
{
/* J'affirme que P->sp est inférieur à N */
    assert (P->sp < N);

    P->T [P->sp] = d;
    P->sp += 1;
/*
    P->T [P->sp++] = d;
 */
}

void depiler (double* d, struct pile* P)
{
    assert (! est_vide_pile (P));

    P->sp -= 1;
    *d = P->T [P->sp];
/*
    *d = P->T [--P->sp];
 */
}

