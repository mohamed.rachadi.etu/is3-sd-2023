/* liste.h */

struct maillon {
    double valeur;
    struct maillon* suivant;
};

#define NIL (struct maillon*)0

struct liste {
    struct maillon* tete;
    int nbelem;
};

/* Spécifications (rapides)
   Le type struct liste implante les listes chaînées.
   La liste vide est codée par tete=NIL et nbelem=0
   Pour les listes non vides,
   - le champ tete pointe vers le premier maillon
   - le champ nbelem contient le nombre de maillons de la liste
   Les maillons sont alloués dynamiquement (dans le tas)
 */

// Constructeur. Initialise L (mode R) à la liste vide
extern void init_liste (struct liste* L);

// Ajoute d (mode D) en tête de L (mode D/R)
extern void ajout_en_tete_liste (struct liste* L, double d);

// Affiche la liste (mode D)
extern void print_liste (struct liste*);

// Le destructeur (mode ??? peut-être D/R)
extern void clear_liste (struct liste*);

