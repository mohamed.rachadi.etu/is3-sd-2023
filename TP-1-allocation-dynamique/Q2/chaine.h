/* chaine.h */

/* 1. La déclaration */

struct chaine{
    char* tab;
    int l;
};

/* 2. Spécifications du type 

   Le type struct chaine permet de représenter des chaine de caracteres 
   Le champ tab  est la chaine de caracteres qui sont allouées dynamiquement 
   Le champ l contient la longueur de la chaine tab.

  
 */

/* 3. Les prototypes des primitives de manipulation du type */

/* Constructeur. initialise la chaine a une chaine vide */
extern void init_chaine(struct chaine*);

/* Ajoute le caractere char en queue de notre chaine */
extern void ajout_en_queue_chaine(struct chaine*, char);

/* Affiche la chaine  */
extern void print_chaine(struct chaine*);

/* Destructeur */
extern void clear(struct chaine*);


