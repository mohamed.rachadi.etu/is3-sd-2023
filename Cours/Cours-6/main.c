// main.c

#include <stdio.h>
#include "abr.h"

int main ()
{   double data [] = { 34, 18, 41, 7, 23, 5 };
    int n = sizeof (data) / sizeof (double);

    struct abr* A;

    A = NIL;
    for (int i = 0; i < n; i++)
    {
        A = ajout_abr (A, data[i]);
    }

    if (recherche_abr (23, A))
        printf ("trouvé\n");
    else
        printf ("pas trouvé\n");

    clear_abr (A);
    return 0;
}

