/* file.h */

#define N 10

struct file {
    double T [N];
    int re;
    int we;
};

/*
 * Spécifications
 * re = l'indice du prochain élément défilé (read end)
 * we = l'indice de la première case libre, derrière le dernier élément
 *  enfilé (write end)
 * les indices sont incrémentés modulo N
 * la file vide se reconnaît à re = we
 */

// Le constructeur
void init_file (struct file*);

// Le destructeur
void clear_file (struct file*);

#include <stdbool.h>

// Retourne true si la file est vide, false sinon
bool est_vide_file (struct file*);

// Emfile le double dans la file
void enfiler (struct file*, double);

// Défile le double en sommet de file
void defiler (double*, struct file*);


