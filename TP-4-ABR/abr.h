
/* DECLARATIONDE LA STRUCTURE ABR */ 
struct abr {
       struct abr* gauche;
       int  valeur;
       struct abr* droite;
       };
#define NIL (struct abr*)0

// Un ABR = un pointeur de type (struct abr*)

// Pas de constructeur - on initialise le pointeur à NIL

// Ajout d'un entier  dans un ABR. 
// La fonction retourne l'ABR obtenu après l'ajout
extern struct abr * ajouter_abr(int ,struct abr *);
// action qui affiche l'arbre comme elle est : 
extern void afficher_abr(struct abr *);
//*extern void afficher_abr_decroissant(struct abr *);  */
// fonction qui donne la hauteur de l'arbre :        
extern int hauteur_abr (struct abr *);
// fonction qui donne le nombre de noeuds :        
extern int nombre_noeuds_abr(struct abr *);
// Le destructeur :       
extern void clear_abr(struct abr * );

extern void affichage_file(struct abr*);       
              
