/* liste.c */

#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

// Constructeur. Initialise L (mode R) à la liste vide
void init_liste (struct liste* L)
{
    L->tete = NIL;
    L->nbelem = 0;
}

// Ajoute d (mode D) en tête de L (mode D/R)
void ajout_en_tete_liste (struct liste* L, double d)
{
// 1.
    struct maillon* M;
    M = (struct maillon*)malloc (sizeof (struct maillon));
// 2.
    M->valeur = d;
// 3.
    M->suivant = L->tete;
// 4.
    L->tete = M;
// 5.
    L->nbelem += 1;
}

// Affiche la liste (mode D)
void print_liste (struct liste* L)
{   struct maillon* M;  // Pas de malloc !!!

    printf ("[");
    M = L->tete;
    for (int i = 0; i < L->nbelem; i++)
    {
        printf ("%lf ", M->valeur);
        M = M->suivant;
    }
    printf ("]\n");
}

/*
   Autre version pour la boucle for (plus simple en un certain sens)

    M = L->tete;
    while (M != NIL)
    {
        printf ("%lf ", M->valeur);
        M = M->suivant;
    }
*/
/*
   Encore une autre version

    for (M = L->tete; M != NIL; M = M->suivant)
    {
        printf ("%lf ", M->valeur);
    }
}
*/

// Le destructeur (mode ??? peut-être D/R)
void clear_liste (struct liste* L)
{
    // Destructeur vide : on a une fuite mémoire !
}

