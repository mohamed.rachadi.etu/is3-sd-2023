/* file.c */

#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
    F->re = F->we = N / 2; /* ou zéro !! */
}

void clear_file (struct file* F)
{
}

bool est_vide_file (struct file* F)
{
    return F->re == F->we;
}

bool est_pleine_file (struct file* F)
{
    return F->re == (F->we + 1) % N;
}

void enfiler (struct file* F, double d)
{
    assert (! est_pleine_file (F));

    F->T [F->we] = d;
    F->we = (F->we + 1) % N;
}

void defiler (double* d, struct file* F)
{
    assert (! est_vide_file (F));

    *d = F->T [F->re];
    F->re = (F->re + 1) % N;
}

