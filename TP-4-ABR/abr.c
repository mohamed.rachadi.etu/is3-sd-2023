/* abr.c */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "abr.h"

// la fonction qui contruit une feuille : 
static struct abr * new_feuille(int a ){
       struct abr * F;
       F=(struct abr *)malloc(sizeof(struct abr));
       F->gauche=NIL;
       F->droite=NIL;
       F->valeur=a;
       return F; }



struct abr* ajouter_abr (int d ,struct abr *A0){
    struct abr* cour;
    struct abr* prec;
    if (A0 == NIL)
        return new_feuille (d);
// On est sûr que l'ABR n'est pas vide
    prec = NIL; 
    cour = A0;
    while (cour != NIL)
    {   prec = cour;
// Pas d'égalité parce que d est une clé
        if (d < cour->valeur)
            cour = cour->gauche;
        else
            cour = cour->droite;
    }
    if (d < prec->valeur)
        prec->gauche = new_feuille (d);
    else
        prec->droite = new_feuille (d);
    return A0;
}

void afficher_abr(struct abr *A){
     if (A!=NIL){
           afficher_abr(A->gauche);
           printf("%d ",A->valeur);
           afficher_abr(A->droite);     
     }
}
/*void afficher_abr_decroissant(struct abr *A){
     while (A!=NIL){
           afficher_abr_decroissant(A->droite);
           printf("%d",A->valeur);
           afficher_abr_decroissant(A->gauche);     
     }
}*/
static int max(int a,int b){
           if (a>b){
              return a ;
           }
           else return b;

}
int hauteur_abr(struct abr*A){
    if (A==NIL){
       return -1;    
    }
    else 
        return 1+max(hauteur_abr(A->droite),hauteur_abr(A->gauche));


}
int nombre_noeuds_abr(struct abr *A){
    if (A==NIL){
       return 0;
    }
    else { return 1+nombre_noeuds_abr(A->droite)+nombre_noeuds_abr(A->gauche) ;}

}
void clear_abr(struct abr *A){
     if (A!=NIL){
         clear_abr(A->gauche);
         clear_abr(A->droite);
         free(A); 
     }
 
}
static bool est_feuille(struct abr*A){
       return (A->gauche==NIL && A->droite==NIL);
}

static void auxilliare_affichage(struct abr*A,FILE *f){
            if (A->gauche!=NIL){
                fprintf(f,"%d->%d [label=\"gauche\"];\n",A->valeur,A->gauche->valeur);
                auxilliare_affichage(A->gauche,f);}
                
            
            
            if (A->droite!=NIL){
                fprintf(f,"%d->%d [label=\"droite\"];\n",A->valeur,A->droite->valeur);
                auxilliare_affichage(A->droite,f);}
                
            
            }



void affichage_file(struct abr *A){
     FILE *f;
     f=fopen("ABR.dot","w");
     assert (f!=(FILE*)0);
     fprintf(f,"digraph G { \n");
     if (A==NIL) {
        fprintf(f,"NIL; \n");
      
     }
     else if (est_feuille(A)) {
             fprintf(f,"%d \n ",A->valeur);
     
     }
     else {auxilliare_affichage(A,f);}
     fprintf(f,"}\n");
     fclose(f);
     system("dot -Tpdf ABR.dot -Grankdir=LR -o ABR.pdf");
     system("evince ABR.pdf");


}

