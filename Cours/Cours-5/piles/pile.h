/* pile.h */

#define N 10

struct pile {
    double T [N];
    int sp;
};

/*
 * Spécifications
 * Le champ sp contient l'indice de la première case libre de T
 * Les cases T[0] à T[sp-1] sont occupées
 * La pile vide se reconnaît à sp = 0
 */

// Le constructeur
void init_pile (struct pile*);

// Le destructeur
void clear_pile (struct pile*);

#include <stdbool.h>

// Retourne true si la pile est vide, false sinon
bool est_vide_pile (struct pile*);

// Empile le double dans la pile
void empiler (struct pile*, double);

// Dépile le double en sommet de pile
void depiler (double*, struct pile*);


