#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 09:44:20 2023

@author: mrachadi
"""

import numpy as np
import matplotlib.pyplot as plt
K = np.loadtxt('Karatsuba.stats')
E = np.loadtxt('Elementaire.stats')

plt.plot (K[:,0], K[:,1], label='Karatsuba')
plt.plot (E[:,0], E[:,1], label='Elementaire')
plt.legend()
plt.show()